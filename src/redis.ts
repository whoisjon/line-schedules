import redis from 'redis';
import { promisify } from 'util';

const client = redis.createClient(6379);

export const get = promisify(client.get).bind(client);

export const set = promisify(client.set).bind(client);

export const keys = promisify(client.keys).bind(client);

console.log('REDIS:', 6379);
