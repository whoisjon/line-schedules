import { Request, Response } from 'express';
import * as redis from './redis';

export async function postLineSchedule(req: Request, res: Response) {
  let { name, times } = req.body;
  // 1. SANITIZE INPUT
  if (!name || !times) return res.status(400).send('missing required fields');
  name = name.toUpperCase();
  if (!Array.isArray(times)) return res.status(400).send('invalid input for times');
  if (!name.match(/^[0-9A-Z]+$/)) return res.status(400).send('invalid input for name');
  if (name.length > 4) return res.status(400).send('invalid input for name');
  let invalid = false;
  const cleanTimes = times.map((time) => {
    const validated = sanitize12hrString(time);
    if (!validated) invalid = true;
    return validated;
  });
  if (invalid) return res.status(400).send('invalid time input');

  const sorted = cleanTimes.sort((a, b) => {
    return <any>new Date(`1970/01/01 ` + a) - <any>new Date('1970/01/01 ' + b);
  });

  const unique = Array.from(new Set(sorted));

  // 2. STORE SCHEDULE
  await redis.set(name, JSON.stringify(unique));

  // 3. CREATE INTERSECTION LOOKUP AND MEMOIZE
  const keys = await redis.keys('*');

  const lineSchedules = await Promise.all(
    keys
      .filter((key) => key !== 'CACHED')
      .map(async (key) => {
        const value = await redis.get(key);
        return JSON.parse(value || '');
      })
  );

  const flatArray = lineSchedules.reduce((a: [], e: []) => {
    a.push(...e);
    return a;
  }, []);

  const uniqueIntersections = flatArray.reduce((a: string[], e: string, i: number) => {
    const index = flatArray.indexOf(e);
    if (index !== i && a.indexOf(e) === -1) a.push(e);
    return a;
  }, []);

  const sortedIntersections = uniqueIntersections.sort((a: any, b: any) => {
    return <any>new Date(`1970/01/01 ` + a) - <any>new Date('1970/01/01 ' + b);
  });

  await redis.set('CACHED', JSON.stringify(sortedIntersections));

  res.send('ok');
}

export async function getNextCongruency(req: Request, res: Response) {
  // 1. SANITIZE INPUT
  let time = <string>req.query.time;
  if (!time) return res.status(400).send('missing required field: time');
  const validated = sanitize12hrString(time);
  if (!validated) return res.status(400).send('invalid input for name');
  const date = new Date(`1970/01/01 ` + validated);

  // 2. RETRIEVE MEMOIZED RESPONSE
  const json = await redis.get('CACHED');
  if (!json) return res.send('no time');
  const timePairs = JSON.parse(json || '');

  // 3. GET NEXT INTERSECTION AFTER DATE
  const filtered = timePairs.filter((e: string) => <any>new Date(`1970/01/01 ` + e) > date);

  res.send(filtered[0] || timePairs[0] || 'no time');
}

// Formats and Validates time strings. Ex: "1:20 PM"
export function sanitize12hrString(time: string) {
  time = time.toUpperCase();
  if (new Date('1970/01/01 ' + time).toString() === 'Invalid Date') return null;
  if (!/^(1[0-2]|0?[1-9]):[0-5][0-9] (AM|PM)$/.test(time)) return null;
  if (time[0] == '0') time = time.replace(/^0+/, '');
  return time;
}
