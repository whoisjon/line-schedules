import mock from 'mock-require';
import assert from 'assert';
import { mockRequest, mockResponse } from 'mock-req-res';

mock('redis', 'redis-mock');

import { getNextCongruency, postLineSchedule } from './controller';

describe('postLineSchedule()', () => {
  it('Should post a schedule for new train lines', async () => {
    const data = [
      {
        name: 'A1',
        times: ['1:00 AM', '2:00 AM', '3:00 AM', '4:00 AM', '5:00 AM'],
      },
      {
        name: 'A2',
        times: ['5:00 AM', '6:00 AM', '7:00 AM', '8:00 AM', '9:00 AM'],
      },
      {
        name: 'A3',
        times: ['9:00 AM', '10:00 AM', '11:00 AM', '12:00 PM', '1:00 PM', '2:00 PM'],
      },
      {
        name: 'A4',
        times: ['2:00 PM', '3:00 PM', '4:00 PM', '5:00 PM', '6:00 PM', '7:00 PM'],
      },
      {
        name: 'A5',
        times: ['7:00 PM', '8:00 PM', '9:00 PM', '10:00 PM', '11:00 PM', '12:00 AM'],
      },
    ];

    await Promise.all(
      data.map(async (body) => {
        const req = mockRequest({ body });
        const res = mockResponse();
        await postLineSchedule(req, res);
        const result = res.send.args[0][0];
        assert(result === 'ok');
      })
    );
  });

  it('Should get the next time multiple trains are going to be arriving at this station in the same minute', async () => {
    const data = [
      {
        input: '3:30 AM',
        expected: '5:00 AM',
      },
      {
        input: '5:30 AM',
        expected: '9:00 AM',
      },
      {
        input: '9:30 AM',
        expected: '2:00 PM',
      },
      {
        input: '2:30 PM',
        expected: '7:00 PM',
      },
      {
        input: '7:30 PM',
        expected: '5:00 AM',
      },
    ];

    await Promise.all(
      data.map(async ({ input, expected }) => {
        const query = { time: input };
        const req = mockRequest({ query });
        const res = mockResponse();
        await getNextCongruency(req, res);
        const result = res.send.args[0][0];
        assert(result === expected);
      })
    );
  });
});
