import express, { Application } from 'express';
import morgan from 'morgan';
import * as controller from './controller';

const app: Application = express();

app.use(express.json());
app.use(morgan('tiny'));

app.get('/next', controller.getNextCongruency);
app.post('/schedule', controller.postLineSchedule);

app.listen(8080, () => console.log('EXPRESS:', 8080));
